import datetime

from smat_cli.smat import Smat


class TestSmat:
    """
    These are all integration tests that make actual calls to the SMAT API, and are thus fragile.
    """

    def test_content(self):
        api = Smat()
        response = api.content(
            limit=8,
            site="telegram",
            since=datetime.date(2022, 1, 1),
            until=datetime.date(2022, 2, 1),
            term="trump",
        )
        assert len(response["hits"]["hits"]) == 8

    def test_timeseries(self):
        api = Smat()
        response = api.timeseries(
            site="telegram",
            since=datetime.date(2022, 1, 1),
            until=datetime.date(2022, 2, 1),
            term="trump",
            interval="day",
        )
        assert len(response["aggregations"]["date"]["buckets"]) == 31

    def test_activity(self):
        api = Smat()
        response = api.activity(
            site="telegram",
            since=datetime.date(2022, 1, 1),
            until=datetime.date(2022, 2, 1),
            term="trump",
            agg_by="channelusername",
        )
        assert len(response["aggregations"]["channelusername"]["buckets"]) == 10
