import itertools
import json

import pytest

from smat_cli.formatters import JsonFormatter, NdjsonFormatter

content_response = {
    "created_key": "date",
    "content_key": "message",
    "took": 4473,
    "timed_out": False,
    "_shards": {"total": 9, "successful": 9, "skipped": 0, "failed": 0},
    "hits": {
        "total": {"value": 10000, "relation": "gte"},
        "max_score": 13.114063,
        "hits": [
            {
                "_index": "smat-telegram-data-000007",
                "_type": "_doc",
                "_id": "6432984586971935598",
                "_score": 13.114063,
                "_source": {
                    "action": {
                        "reformatted_key": "MessageActionChatAddUser",
                        "users": [1476513169],
                    },
                    "archivedon": "2022-03-05 19:20:52.44944",
                    "channeltitle": "JuliansRum Chat",
                    "channelusername": "juliansrumchat",
                    "date": 1643590683,
                    "fromid": {"reformatted_key": "PeerUser", "user_id": 1476513169},
                    "peerid": {
                        "channel_id": 1497795942,
                        "reformatted_key": "PeerChannel",
                    },
                    "post": False,
                    "reformattedkey": "MessageService",
                    "replyto": None,
                    "silent": False,
                    "userinfo": {
                        "_id": 1476513169,
                        "_v": 1,
                        "access_hash": -4460633508820071640,
                        "apply_min_photo": True,
                        "bot": False,
                        "bot_chat_history": False,
                        "bot_info_version": None,
                        "bot_inline_geo": False,
                        "bot_inline_placeholder": None,
                        "bot_nochats": False,
                        "contact": False,
                        "deleted": False,
                        "first_name": "Pro",
                        "id": 1476513169,
                        "is_self": False,
                        "lang_code": None,
                        "last_name": "trump",
                        "min": False,
                        "mutual_contact": False,
                        "phone": None,
                        "photo": None,
                        "reformatted_key": "User",
                        "restricted": False,
                        "restriction_reason": [],
                        "scam": False,
                        "status": {"reformatted_key": "UserStatusRecently"},
                        "support": False,
                        "username": "wilren",
                        "verified": False,
                    },
                    "v": 1,
                },
            },
            {
                "_index": "smat-telegram-data-000007",
                "_type": "_doc",
                "_id": "5495110243744498248",
                "_score": 12.265501,
                "_source": {
                    "archivedon": "2022-01-09 19:49:45.82526",
                    "channelabout": "⚠️ Warning: Many users reported this account as a scam or a fake account. Please be careful, especially if it asks you for money.",
                    "channeltitle": "Donald J. Trump",
                    "channelusername": "trump",
                    "date": 1641752956,
                    "editdate": None,
                    "edithide": False,
                    "entities": [
                        {
                            "length": 90,
                            "offset": 3,
                            "reformatted_key": "MessageEntityItalic",
                        }
                    ],
                    "forwards": 1,
                    "fromid": None,
                    "fwdfrom": None,
                    "groupedid": None,
                    "message": "⚠️ Warning: Many users report that this account impersonates a famous person or organisation.",
                    "peerid": {
                        "channel_id": 1279430055,
                        "reformatted_key": "PeerChannel",
                    },
                    "pinned": False,
                    "post": True,
                    "postauthor": None,
                    "reformattedkey": "Message",
                    "replies": None,
                    "replyto": None,
                    "silent": False,
                    "v": 1,
                    "viabotid": None,
                    "views": 1850,
                },
            },
            {
                "_index": "smat-telegram-data-000007",
                "_type": "_doc",
                "_id": "5495110243744498246",
                "_score": 12.265501,
                "_ignored": ["message.keyword"],
                "_source": {
                    "archivedon": "2022-01-09 19:49:45.823823",
                    "channelabout": "⚠️ Warning: Many users reported this account as a scam or a fake account. Please be careful, especially if it asks you for money.",
                    "channeltitle": "Donald J. Trump",
                    "channelusername": "trump",
                    "date": 1641746683,
                    "editdate": None,
                    "edithide": False,
                    "entities": [
                        {
                            "length": 17,
                            "offset": 0,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 1,
                            "offset": 41,
                            "reformatted_key": "MessageEntityItalic",
                        },
                        {
                            "length": 7,
                            "offset": 43,
                            "reformatted_key": "MessageEntityItalic",
                        },
                        {
                            "length": 20,
                            "offset": 57,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 28,
                            "offset": 80,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 28,
                            "offset": 80,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 28,
                            "offset": 112,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 28,
                            "offset": 112,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 13,
                            "offset": 144,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 44,
                            "offset": 158,
                            "reformatted_key": "MessageEntityCode",
                        },
                        {
                            "length": 33,
                            "offset": 205,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 90,
                            "offset": 239,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 90,
                            "offset": 239,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 11,
                            "offset": 334,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 6,
                            "offset": 346,
                            "reformatted_key": "MessageEntityMention",
                        },
                        {
                            "length": 7,
                            "offset": 355,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 22,
                            "offset": 362,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 18,
                            "offset": 362,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 9,
                            "offset": 392,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 11,
                            "offset": 402,
                            "reformatted_key": "MessageEntityMention",
                        },
                        {
                            "length": 90,
                            "offset": 418,
                            "reformatted_key": "MessageEntityItalic",
                        },
                    ],
                    "forwards": 2,
                    "fromid": None,
                    "fwdfrom": {
                        "channel_post": 16793,
                        "date": 1640977195,
                        "from_id": {
                            "channel_id": 1279430055,
                            "reformatted_key": "PeerChannel",
                        },
                        "from_name": None,
                        "post_author": None,
                        "psa_type": None,
                        "reformatted_key": "MessageFwdHeader",
                        "saved_from_msg_id": None,
                        "saved_from_peer": None,
                    },
                    "groupedid": None,
                    "media": None,
                    "message": "IS IT RESTAURANT? 🥳 SURE! WE WILL MAKE +2 000 000%.\n\n🚨 Buying instruction:\n1. https://youtu.be/GWXj2hi_-bI\n2. https://youtu.be/aQX1NMR5A98\n\n✅ BSC Contract:\n0xf0c4FcAd6597EcA8882d2be8724d31f56bf5D9fd\n\n🧿 Link for purchase on PancakeSwap:\nhttps://pancakeswap.finance/swap?outputCurrency=0xf0c4FcAd6597EcA8882d2be8724d31f56bf5D9fd\n\n📌 TG Channel: @trump\n🔱 Site: https://cointrump.info\n👨🏻\u200d💻 Support: @trump_supp\n\n⚠️ Warning: Many users report that this account impersonates a famous person or organisation.",
                    "peerid": {
                        "channel_id": 1279430055,
                        "reformatted_key": "PeerChannel",
                    },
                    "pinned": False,
                    "post": True,
                    "postauthor": None,
                    "reformattedkey": "Message",
                    "replies": None,
                    "replyto": None,
                    "silent": False,
                    "v": 1,
                    "viabotid": None,
                    "views": 3325,
                },
            },
            {
                "_index": "smat-telegram-data-000007",
                "_type": "_doc",
                "_id": "5495110243744498245",
                "_score": 12.265501,
                "_source": {
                    "archivedon": "2022-01-09 19:49:45.823833",
                    "channelabout": "⚠️ Warning: Many users reported this account as a scam or a fake account. Please be careful, especially if it asks you for money.",
                    "channeltitle": "Donald J. Trump",
                    "channelusername": "trump",
                    "date": 1641741684,
                    "editdate": None,
                    "edithide": False,
                    "entities": [
                        {
                            "length": 90,
                            "offset": 3,
                            "reformatted_key": "MessageEntityItalic",
                        }
                    ],
                    "forwards": 6,
                    "fromid": None,
                    "fwdfrom": None,
                    "groupedid": None,
                    "message": "⚠️ Warning: Many users report that this account impersonates a famous person or organisation.",
                    "peerid": {
                        "channel_id": 1279430055,
                        "reformatted_key": "PeerChannel",
                    },
                    "pinned": False,
                    "post": True,
                    "postauthor": None,
                    "reformattedkey": "Message",
                    "replies": None,
                    "replyto": None,
                    "silent": False,
                    "v": 1,
                    "viabotid": None,
                    "views": 4413,
                },
            },
            {
                "_index": "smat-telegram-data-000007",
                "_type": "_doc",
                "_id": "5495110243744498204",
                "_score": 12.265501,
                "_ignored": ["message.keyword"],
                "_source": {
                    "archivedon": "2022-01-09 19:49:45.821673",
                    "channelabout": "⚠️ Warning: Many users reported this account as a scam or a fake account. Please be careful, especially if it asks you for money.",
                    "channeltitle": "Donald J. Trump",
                    "channelusername": "trump",
                    "date": 1641553795,
                    "editdate": None,
                    "edithide": False,
                    "entities": [
                        {
                            "length": 10,
                            "offset": 18,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 10,
                            "offset": 205,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 20,
                            "offset": 260,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 28,
                            "offset": 283,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 28,
                            "offset": 283,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 28,
                            "offset": 315,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 28,
                            "offset": 315,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 13,
                            "offset": 347,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 44,
                            "offset": 361,
                            "reformatted_key": "MessageEntityCode",
                        },
                        {
                            "length": 33,
                            "offset": 408,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 90,
                            "offset": 442,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 90,
                            "offset": 442,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 11,
                            "offset": 537,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 6,
                            "offset": 549,
                            "reformatted_key": "MessageEntityMention",
                        },
                        {
                            "length": 6,
                            "offset": 549,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 7,
                            "offset": 558,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 22,
                            "offset": 565,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 22,
                            "offset": 565,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 9,
                            "offset": 595,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 11,
                            "offset": 605,
                            "reformatted_key": "MessageEntityMention",
                        },
                        {
                            "length": 11,
                            "offset": 605,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 90,
                            "offset": 621,
                            "reformatted_key": "MessageEntityItalic",
                        },
                    ],
                    "forwards": 24,
                    "fromid": None,
                    "fwdfrom": None,
                    "groupedid": None,
                    "message": "⚠️ Friends! Today TRIMP COIN has passed a reputable security audit from TechRate.\n\nThis means that today we are on the first line with such projects as ETH, SOL and XRP. Your investments are protected and TRUMP COIN is completely decentralized and secure.\n\n🚨 Buying instruction:\n1. https://youtu.be/GWXj2hi_-bI\n2. https://youtu.be/aQX1NMR5A98\n\n✅ BSC Contract:\n0xf0c4FcAd6597EcA8882d2be8724d31f56bf5D9fd\n\n🧿 Link for purchase on PancakeSwap:\nhttps://pancakeswap.finance/swap?outputCurrency=0xf0c4FcAd6597EcA8882d2be8724d31f56bf5D9fd\n\n📌 TG Channel: @trump\n🔱 Site: https://cointrump.info\n👨🏻\u200d💻 Support: @trump_supp\n\n⚠️ Warning: Many users report that this account impersonates a famous person or organisation.",
                    "peerid": {
                        "channel_id": 1279430055,
                        "reformatted_key": "PeerChannel",
                    },
                    "pinned": False,
                    "post": True,
                    "postauthor": None,
                    "reformattedkey": "Message",
                    "replies": None,
                    "replyto": None,
                    "silent": False,
                    "v": 1,
                    "viabotid": None,
                    "views": 13581,
                },
            },
            {
                "_index": "smat-telegram-data-000007",
                "_type": "_doc",
                "_id": "5495110243744498203",
                "_score": 12.265501,
                "_source": {
                    "archivedon": "2022-01-09 19:49:45.820251",
                    "channelabout": "⚠️ Warning: Many users reported this account as a scam or a fake account. Please be careful, especially if it asks you for money.",
                    "channeltitle": "Donald J. Trump",
                    "channelusername": "trump",
                    "date": 1641552459,
                    "editdate": None,
                    "edithide": False,
                    "entities": [
                        {
                            "length": 90,
                            "offset": 3,
                            "reformatted_key": "MessageEntityItalic",
                        }
                    ],
                    "forwards": 2,
                    "fromid": None,
                    "fwdfrom": None,
                    "groupedid": None,
                    "message": "⚠️ Warning: Many users report that this account impersonates a famous person or organisation.",
                    "peerid": {
                        "channel_id": 1279430055,
                        "reformatted_key": "PeerChannel",
                    },
                    "pinned": False,
                    "post": True,
                    "postauthor": None,
                    "reformattedkey": "Message",
                    "replies": None,
                    "replyto": None,
                    "silent": False,
                    "v": 1,
                    "viabotid": None,
                    "views": 11138,
                },
            },
            {
                "_index": "smat-telegram-data-000007",
                "_type": "_doc",
                "_id": "5495110243744498196",
                "_score": 12.265501,
                "_ignored": ["message.keyword"],
                "_source": {
                    "archivedon": "2022-01-09 19:49:45.820312",
                    "channelabout": "⚠️ Warning: Many users reported this account as a scam or a fake account. Please be careful, especially if it asks you for money.",
                    "channeltitle": "Donald J. Trump",
                    "channelusername": "trump",
                    "date": 1641512194,
                    "editdate": 1641746627,
                    "edithide": False,
                    "entities": [
                        {
                            "length": 39,
                            "offset": 0,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 63,
                            "offset": 185,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 20,
                            "offset": 360,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 28,
                            "offset": 383,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 28,
                            "offset": 383,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 28,
                            "offset": 415,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 28,
                            "offset": 415,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 13,
                            "offset": 447,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 44,
                            "offset": 461,
                            "reformatted_key": "MessageEntityCode",
                        },
                        {
                            "length": 33,
                            "offset": 508,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 90,
                            "offset": 542,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 90,
                            "offset": 542,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 11,
                            "offset": 637,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 6,
                            "offset": 649,
                            "reformatted_key": "MessageEntityMention",
                        },
                        {
                            "length": 6,
                            "offset": 649,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 7,
                            "offset": 658,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 22,
                            "offset": 665,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 22,
                            "offset": 665,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 9,
                            "offset": 695,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 11,
                            "offset": 705,
                            "reformatted_key": "MessageEntityMention",
                        },
                        {
                            "length": 11,
                            "offset": 705,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 90,
                            "offset": 721,
                            "reformatted_key": "MessageEntityItalic",
                        },
                    ],
                    "forwards": 33,
                    "fromid": None,
                    "fwdfrom": None,
                    "groupedid": None,
                    "message": "🔰 Official White Paper of TRUMP COIN. This is a necessary financial and investment invitation for serious investors before listing on the BINANCE exchange.\n\nLet me remind you that the listing on the BINANCE exchange will take place on January 15th. All investors will be notified of the details in advance. Thank you for your trust, support and interest!\n\n🚨 Buying instruction:\n1. https://youtu.be/GWXj2hi_-bI\n2. https://youtu.be/aQX1NMR5A98\n\n✅ BSC Contract:\n0xf0c4FcAd6597EcA8882d2be8724d31f56bf5D9fd\n\n🧿 Link for purchase on PancakeSwap:\nhttps://pancakeswap.finance/swap?outputCurrency=0xf0c4FcAd6597EcA8882d2be8724d31f56bf5D9fd\n\n📌 TG Channel: @trump\n🔱 Site: https://cointrump.info\n👨🏻\u200d💻 Support: @trump_supp\n\n⚠️ Warning: Many users report that this account impersonates a famous person or organisation.",
                    "peerid": {
                        "channel_id": 1279430055,
                        "reformatted_key": "PeerChannel",
                    },
                    "pinned": False,
                    "post": True,
                    "postauthor": None,
                    "reformattedkey": "Message",
                    "replies": None,
                    "replyto": None,
                    "silent": False,
                    "v": 1,
                    "viabotid": None,
                    "views": 15937,
                },
            },
            {
                "_index": "smat-telegram-data-000007",
                "_type": "_doc",
                "_id": "5495110243744498136",
                "_score": 12.265501,
                "_ignored": ["message.keyword"],
                "_source": {
                    "archivedon": "2022-01-09 19:49:45.818764",
                    "channelabout": "⚠️ Warning: Many users reported this account as a scam or a fake account. Please be careful, especially if it asks you for money.",
                    "channeltitle": "Donald J. Trump",
                    "channelusername": "trump",
                    "date": 1641249483,
                    "editdate": None,
                    "edithide": False,
                    "entities": [
                        {
                            "length": 12,
                            "offset": 105,
                            "reformatted_key": "MessageEntityTextUrl",
                            "url": "https://www.opec.org/opec_web/en/",
                        },
                        {
                            "length": 10,
                            "offset": 132,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 12,
                            "offset": 249,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 20,
                            "offset": 345,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 28,
                            "offset": 368,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 28,
                            "offset": 368,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 28,
                            "offset": 400,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 28,
                            "offset": 400,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 13,
                            "offset": 432,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 44,
                            "offset": 446,
                            "reformatted_key": "MessageEntityCode",
                        },
                        {
                            "length": 33,
                            "offset": 493,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 90,
                            "offset": 527,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 90,
                            "offset": 527,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 11,
                            "offset": 622,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 6,
                            "offset": 634,
                            "reformatted_key": "MessageEntityMention",
                        },
                        {
                            "length": 6,
                            "offset": 634,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 7,
                            "offset": 643,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 22,
                            "offset": 650,
                            "reformatted_key": "MessageEntityUrl",
                        },
                        {
                            "length": 22,
                            "offset": 650,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 9,
                            "offset": 680,
                            "reformatted_key": "MessageEntityBold",
                        },
                        {
                            "length": 11,
                            "offset": 690,
                            "reformatted_key": "MessageEntityMention",
                        },
                        {
                            "length": 11,
                            "offset": 690,
                            "reformatted_key": "MessageEntityUnderline",
                        },
                        {
                            "length": 90,
                            "offset": 706,
                            "reformatted_key": "MessageEntityItalic",
                        },
                    ],
                    "forwards": 21,
                    "fromid": None,
                    "fwdfrom": None,
                    "groupedid": None,
                    "media": None,
                    "message": "🚀 Tomorrow we expect a frantic rise in the exchange rate due to the fact that tomorrow there will be an OPEC meeting, at which the TRUMP COIN will be mentioned. \n\n+ An animated video will also be released on YouTube in support of the project soon. Donald Trump will soon publicly announce the open recruitment of investors for the project.\n\n🚨 Buying instruction:\n1. https://youtu.be/GWXj2hi_-bI\n2. https://youtu.be/aQX1NMR5A98\n\n✅ BSC Contract:\n0xf0c4FcAd6597EcA8882d2be8724d31f56bf5D9fd\n\n🧿 Link for purchase on PancakeSwap:\nhttps://pancakeswap.finance/swap?outputCurrency=0xf0c4FcAd6597EcA8882d2be8724d31f56bf5D9fd\n\n📌 TG Channel: @trump\n🔱 Site: https://cointrump.info\n👨🏻\u200d💻 Support: @trump_supp\n\n⚠️ Warning: Many users report that this account impersonates a famous person or organisation.",
                    "peerid": {
                        "channel_id": 1279430055,
                        "reformatted_key": "PeerChannel",
                    },
                    "pinned": False,
                    "post": True,
                    "postauthor": None,
                    "reformattedkey": "Message",
                    "replies": None,
                    "replyto": None,
                    "silent": False,
                    "v": 1,
                    "viabotid": None,
                    "views": 22193,
                },
            },
        ],
    },
}

timeseries_response = {
    "created_key": "date",
    "took": 824,
    "timed_out": False,
    "_shards": {"total": 9, "successful": 9, "skipped": 0, "failed": 0},
    "hits": {
        "total": {"value": 10000, "relation": "gte"},
        "max_score": None,
        "hits": [],
    },
    "aggregations": {
        "date": {
            "buckets": [
                {
                    "key_as_string": "1/1/22(Sat)00:00:00",
                    "key": 1640995200000,
                    "doc_count": 898,
                },
                {
                    "key_as_string": "1/2/22(Sun)00:00:00",
                    "key": 1641081600000,
                    "doc_count": 1207,
                },
                {
                    "key_as_string": "1/3/22(Mon)00:00:00",
                    "key": 1641168000000,
                    "doc_count": 1412,
                },
                {
                    "key_as_string": "1/4/22(Tue)00:00:00",
                    "key": 1641254400000,
                    "doc_count": 1390,
                },
                {
                    "key_as_string": "1/5/22(Wed)00:00:00",
                    "key": 1641340800000,
                    "doc_count": 1483,
                },
                {
                    "key_as_string": "1/6/22(Thu)00:00:00",
                    "key": 1641427200000,
                    "doc_count": 1640,
                },
                {
                    "key_as_string": "1/7/22(Fri)00:00:00",
                    "key": 1641513600000,
                    "doc_count": 1415,
                },
                {
                    "key_as_string": "1/8/22(Sat)00:00:00",
                    "key": 1641600000000,
                    "doc_count": 1656,
                },
                {
                    "key_as_string": "1/9/22(Sun)00:00:00",
                    "key": 1641686400000,
                    "doc_count": 1645,
                },
                {
                    "key_as_string": "1/10/22(Mon)00:00:00",
                    "key": 1641772800000,
                    "doc_count": 1495,
                },
                {
                    "key_as_string": "1/11/22(Tue)00:00:00",
                    "key": 1641859200000,
                    "doc_count": 1608,
                },
                {
                    "key_as_string": "1/12/22(Wed)00:00:00",
                    "key": 1641945600000,
                    "doc_count": 1552,
                },
                {
                    "key_as_string": "1/13/22(Thu)00:00:00",
                    "key": 1642032000000,
                    "doc_count": 2299,
                },
                {
                    "key_as_string": "1/14/22(Fri)00:00:00",
                    "key": 1642118400000,
                    "doc_count": 2166,
                },
                {
                    "key_as_string": "1/15/22(Sat)00:00:00",
                    "key": 1642204800000,
                    "doc_count": 2315,
                },
                {
                    "key_as_string": "1/16/22(Sun)00:00:00",
                    "key": 1642291200000,
                    "doc_count": 3559,
                },
                {
                    "key_as_string": "1/17/22(Mon)00:00:00",
                    "key": 1642377600000,
                    "doc_count": 3244,
                },
                {
                    "key_as_string": "1/18/22(Tue)00:00:00",
                    "key": 1642464000000,
                    "doc_count": 2975,
                },
                {
                    "key_as_string": "1/19/22(Wed)00:00:00",
                    "key": 1642550400000,
                    "doc_count": 2183,
                },
                {
                    "key_as_string": "1/20/22(Thu)00:00:00",
                    "key": 1642636800000,
                    "doc_count": 1957,
                },
                {
                    "key_as_string": "1/21/22(Fri)00:00:00",
                    "key": 1642723200000,
                    "doc_count": 2508,
                },
                {
                    "key_as_string": "1/22/22(Sat)00:00:00",
                    "key": 1642809600000,
                    "doc_count": 1913,
                },
                {
                    "key_as_string": "1/23/22(Sun)00:00:00",
                    "key": 1642896000000,
                    "doc_count": 1687,
                },
                {
                    "key_as_string": "1/24/22(Mon)00:00:00",
                    "key": 1642982400000,
                    "doc_count": 2673,
                },
                {
                    "key_as_string": "1/25/22(Tue)00:00:00",
                    "key": 1643068800000,
                    "doc_count": 1818,
                },
                {
                    "key_as_string": "1/26/22(Wed)00:00:00",
                    "key": 1643155200000,
                    "doc_count": 2024,
                },
                {
                    "key_as_string": "1/27/22(Thu)00:00:00",
                    "key": 1643241600000,
                    "doc_count": 1691,
                },
                {
                    "key_as_string": "1/28/22(Fri)00:00:00",
                    "key": 1643328000000,
                    "doc_count": 1609,
                },
                {
                    "key_as_string": "1/29/22(Sat)00:00:00",
                    "key": 1643414400000,
                    "doc_count": 1833,
                },
                {
                    "key_as_string": "1/30/22(Sun)00:00:00",
                    "key": 1643500800000,
                    "doc_count": 4040,
                },
                {
                    "key_as_string": "1/31/22(Mon)00:00:00",
                    "key": 1643587200000,
                    "doc_count": 2300,
                },
            ]
        }
    },
}

timeseries_with_changepoint_response = {
    "created_key": "date",
    "took": 1277,
    "timed_out": False,
    "_shards": {"total": 9, "successful": 9, "skipped": 0, "failed": 0},
    "hits": {
        "total": {"value": 10000, "relation": "gte"},
        "max_score": None,
        "hits": [],
    },
    "aggregations": {
        "date": {
            "buckets": [
                {
                    "key_as_string": "1/6/21(Wed)00:00:00",
                    "key": 1609891200000,
                    "doc_count": 32299,
                },
                {
                    "key_as_string": "1/7/21(Thu)00:00:00",
                    "key": 1609977600000,
                    "doc_count": 33816,
                },
                {
                    "key_as_string": "1/8/21(Fri)00:00:00",
                    "key": 1610064000000,
                    "doc_count": 25101,
                },
                {
                    "key_as_string": "1/9/21(Sat)00:00:00",
                    "key": 1610150400000,
                    "doc_count": 26559,
                },
                {
                    "key_as_string": "1/10/21(Sun)00:00:00",
                    "key": 1610236800000,
                    "doc_count": 26161,
                },
                {
                    "key_as_string": "1/11/21(Mon)00:00:00",
                    "key": 1610323200000,
                    "doc_count": 34223,
                },
                {
                    "key_as_string": "1/12/21(Tue)00:00:00",
                    "key": 1610409600000,
                    "doc_count": 34962,
                },
                {
                    "key_as_string": "1/13/21(Wed)00:00:00",
                    "key": 1610496000000,
                    "doc_count": 32322,
                },
                {
                    "key_as_string": "1/14/21(Thu)00:00:00",
                    "key": 1610582400000,
                    "doc_count": 29837,
                },
                {
                    "key_as_string": "1/15/21(Fri)00:00:00",
                    "key": 1610668800000,
                    "doc_count": 25521,
                },
                {
                    "key_as_string": "1/16/21(Sat)00:00:00",
                    "key": 1610755200000,
                    "doc_count": 23304,
                },
                {
                    "key_as_string": "1/17/21(Sun)00:00:00",
                    "key": 1610841600000,
                    "doc_count": 23746,
                },
                {
                    "key_as_string": "1/18/21(Mon)00:00:00",
                    "key": 1610928000000,
                    "doc_count": 24619,
                },
                {
                    "key_as_string": "1/19/21(Tue)00:00:00",
                    "key": 1611014400000,
                    "doc_count": 29631,
                },
                {
                    "key_as_string": "1/20/21(Wed)00:00:00",
                    "key": 1611100800000,
                    "doc_count": 51935,
                },
                {
                    "key_as_string": "1/21/21(Thu)00:00:00",
                    "key": 1611187200000,
                    "doc_count": 30623,
                },
                {
                    "key_as_string": "1/22/21(Fri)00:00:00",
                    "key": 1611273600000,
                    "doc_count": 27191,
                },
                {
                    "key_as_string": "1/23/21(Sat)00:00:00",
                    "key": 1611360000000,
                    "doc_count": 20558,
                },
                {
                    "key_as_string": "1/24/21(Sun)00:00:00",
                    "key": 1611446400000,
                    "doc_count": 18975,
                },
                {
                    "key_as_string": "1/25/21(Mon)00:00:00",
                    "key": 1611532800000,
                    "doc_count": 20951,
                },
                {
                    "key_as_string": "1/26/21(Tue)00:00:00",
                    "key": 1611619200000,
                    "doc_count": 23324,
                },
                {
                    "key_as_string": "1/27/21(Wed)00:00:00",
                    "key": 1611705600000,
                    "doc_count": 18171,
                },
                {
                    "key_as_string": "1/28/21(Thu)00:00:00",
                    "key": 1611792000000,
                    "doc_count": 17580,
                },
                {
                    "key_as_string": "1/29/21(Fri)00:00:00",
                    "key": 1611878400000,
                    "doc_count": 14873,
                },
                {
                    "key_as_string": "1/30/21(Sat)00:00:00",
                    "key": 1611964800000,
                    "doc_count": 14063,
                },
                {
                    "key_as_string": "1/31/21(Sun)00:00:00",
                    "key": 1612051200000,
                    "doc_count": 12816,
                },
                {
                    "key_as_string": "2/1/21(Mon)00:00:00",
                    "key": 1612137600000,
                    "doc_count": 16462,
                },
                {
                    "key_as_string": "2/2/21(Tue)00:00:00",
                    "key": 1612224000000,
                    "doc_count": 15650,
                },
                {
                    "key_as_string": "2/3/21(Wed)00:00:00",
                    "key": 1612310400000,
                    "doc_count": 15211,
                },
                {
                    "key_as_string": "2/4/21(Thu)00:00:00",
                    "key": 1612396800000,
                    "doc_count": 16556,
                },
                {
                    "key_as_string": "2/5/21(Fri)00:00:00",
                    "key": 1612483200000,
                    "doc_count": 15950,
                },
                {
                    "key_as_string": "2/6/21(Sat)00:00:00",
                    "key": 1612569600000,
                    "doc_count": 16451,
                },
                {
                    "key_as_string": "2/7/21(Sun)00:00:00",
                    "key": 1612656000000,
                    "doc_count": 15094,
                },
                {
                    "key_as_string": "2/8/21(Mon)00:00:00",
                    "key": 1612742400000,
                    "doc_count": 16585,
                },
                {
                    "key_as_string": "2/9/21(Tue)00:00:00",
                    "key": 1612828800000,
                    "doc_count": 16915,
                },
                {
                    "key_as_string": "2/10/21(Wed)00:00:00",
                    "key": 1612915200000,
                    "doc_count": 16239,
                },
                {
                    "key_as_string": "2/11/21(Thu)00:00:00",
                    "key": 1613001600000,
                    "doc_count": 15952,
                },
                {
                    "key_as_string": "2/12/21(Fri)00:00:00",
                    "key": 1613088000000,
                    "doc_count": 11865,
                },
                {
                    "key_as_string": "2/13/21(Sat)00:00:00",
                    "key": 1613174400000,
                    "doc_count": 14678,
                },
                {
                    "key_as_string": "2/14/21(Sun)00:00:00",
                    "key": 1613260800000,
                    "doc_count": 11389,
                },
                {
                    "key_as_string": "2/15/21(Mon)00:00:00",
                    "key": 1613347200000,
                    "doc_count": 11306,
                },
                {
                    "key_as_string": "2/16/21(Tue)00:00:00",
                    "key": 1613433600000,
                    "doc_count": 10110,
                },
                {
                    "key_as_string": "2/17/21(Wed)00:00:00",
                    "key": 1613520000000,
                    "doc_count": 9562,
                },
                {
                    "key_as_string": "2/18/21(Thu)00:00:00",
                    "key": 1613606400000,
                    "doc_count": 8978,
                },
                {
                    "key_as_string": "2/19/21(Fri)00:00:00",
                    "key": 1613692800000,
                    "doc_count": 7940,
                },
                {
                    "key_as_string": "2/20/21(Sat)00:00:00",
                    "key": 1613779200000,
                    "doc_count": 6720,
                },
                {
                    "key_as_string": "2/21/21(Sun)00:00:00",
                    "key": 1613865600000,
                    "doc_count": 5791,
                },
                {
                    "key_as_string": "2/22/21(Mon)00:00:00",
                    "key": 1613952000000,
                    "doc_count": 7063,
                },
                {
                    "key_as_string": "2/23/21(Tue)00:00:00",
                    "key": 1614038400000,
                    "doc_count": 6018,
                },
                {
                    "key_as_string": "2/24/21(Wed)00:00:00",
                    "key": 1614124800000,
                    "doc_count": 5682,
                },
                {
                    "key_as_string": "2/25/21(Thu)00:00:00",
                    "key": 1614211200000,
                    "doc_count": 5383,
                },
                {
                    "key_as_string": "2/26/21(Fri)00:00:00",
                    "key": 1614297600000,
                    "doc_count": 6458,
                },
                {
                    "key_as_string": "2/27/21(Sat)00:00:00",
                    "key": 1614384000000,
                    "doc_count": 6413,
                },
                {
                    "key_as_string": "2/28/21(Sun)00:00:00",
                    "key": 1614470400000,
                    "doc_count": 8971,
                },
            ]
        },
        "changepoint": {
            "change_date": 1611619200000,
            "date": {
                "before": [
                    {
                        "key": 1609891200000,
                        "key_as_string": "1609891200",
                        "doc_count": 32299,
                    },
                    {
                        "key": 1609977600000,
                        "key_as_string": "1609977600",
                        "doc_count": 33816,
                    },
                    {
                        "key": 1610064000000,
                        "key_as_string": "1610064000",
                        "doc_count": 25101,
                    },
                    {
                        "key": 1610150400000,
                        "key_as_string": "1610150400",
                        "doc_count": 26559,
                    },
                    {
                        "key": 1610236800000,
                        "key_as_string": "1610236800",
                        "doc_count": 26161,
                    },
                    {
                        "key": 1610323200000,
                        "key_as_string": "1610323200",
                        "doc_count": 34223,
                    },
                    {
                        "key": 1610409600000,
                        "key_as_string": "1610409600",
                        "doc_count": 34962,
                    },
                    {
                        "key": 1610496000000,
                        "key_as_string": "1610496000",
                        "doc_count": 32322,
                    },
                    {
                        "key": 1610582400000,
                        "key_as_string": "1610582400",
                        "doc_count": 29837,
                    },
                    {
                        "key": 1610668800000,
                        "key_as_string": "1610668800",
                        "doc_count": 25521,
                    },
                    {
                        "key": 1610755200000,
                        "key_as_string": "1610755200",
                        "doc_count": 23304,
                    },
                    {
                        "key": 1610841600000,
                        "key_as_string": "1610841600",
                        "doc_count": 23746,
                    },
                    {
                        "key": 1610928000000,
                        "key_as_string": "1610928000",
                        "doc_count": 24619,
                    },
                    {
                        "key": 1611014400000,
                        "key_as_string": "1611014400",
                        "doc_count": 29631,
                    },
                    {
                        "key": 1611100800000,
                        "key_as_string": "1611100800",
                        "doc_count": 51935,
                    },
                    {
                        "key": 1611187200000,
                        "key_as_string": "1611187200",
                        "doc_count": 30623,
                    },
                    {
                        "key": 1611273600000,
                        "key_as_string": "1611273600",
                        "doc_count": 27191,
                    },
                    {
                        "key": 1611360000000,
                        "key_as_string": "1611360000",
                        "doc_count": 20558,
                    },
                    {
                        "key": 1611446400000,
                        "key_as_string": "1611446400",
                        "doc_count": 18975,
                    },
                    {
                        "key": 1611532800000,
                        "key_as_string": "1611532800",
                        "doc_count": 20951,
                    },
                    {
                        "key": 1611619200000,
                        "key_as_string": "1611619200",
                        "doc_count": 23324,
                    },
                ],
                "after": [
                    {
                        "key": 1611705600000,
                        "key_as_string": "1611705600",
                        "doc_count": 18171,
                    },
                    {
                        "key": 1611792000000,
                        "key_as_string": "1611792000",
                        "doc_count": 17580,
                    },
                    {
                        "key": 1611878400000,
                        "key_as_string": "1611878400",
                        "doc_count": 14873,
                    },
                    {
                        "key": 1611964800000,
                        "key_as_string": "1611964800",
                        "doc_count": 14063,
                    },
                    {
                        "key": 1612051200000,
                        "key_as_string": "1612051200",
                        "doc_count": 12816,
                    },
                    {
                        "key": 1612137600000,
                        "key_as_string": "1612137600",
                        "doc_count": 16462,
                    },
                    {
                        "key": 1612224000000,
                        "key_as_string": "1612224000",
                        "doc_count": 15650,
                    },
                    {
                        "key": 1612310400000,
                        "key_as_string": "1612310400",
                        "doc_count": 15211,
                    },
                    {
                        "key": 1612396800000,
                        "key_as_string": "1612396800",
                        "doc_count": 16556,
                    },
                    {
                        "key": 1612483200000,
                        "key_as_string": "1612483200",
                        "doc_count": 15950,
                    },
                    {
                        "key": 1612569600000,
                        "key_as_string": "1612569600",
                        "doc_count": 16451,
                    },
                    {
                        "key": 1612656000000,
                        "key_as_string": "1612656000",
                        "doc_count": 15094,
                    },
                    {
                        "key": 1612742400000,
                        "key_as_string": "1612742400",
                        "doc_count": 16585,
                    },
                    {
                        "key": 1612828800000,
                        "key_as_string": "1612828800",
                        "doc_count": 16915,
                    },
                    {
                        "key": 1612915200000,
                        "key_as_string": "1612915200",
                        "doc_count": 16239,
                    },
                    {
                        "key": 1613001600000,
                        "key_as_string": "1613001600",
                        "doc_count": 15952,
                    },
                    {
                        "key": 1613088000000,
                        "key_as_string": "1613088000",
                        "doc_count": 11865,
                    },
                    {
                        "key": 1613174400000,
                        "key_as_string": "1613174400",
                        "doc_count": 14678,
                    },
                    {
                        "key": 1613260800000,
                        "key_as_string": "1613260800",
                        "doc_count": 11389,
                    },
                    {
                        "key": 1613347200000,
                        "key_as_string": "1613347200",
                        "doc_count": 11306,
                    },
                    {
                        "key": 1613433600000,
                        "key_as_string": "1613433600",
                        "doc_count": 10110,
                    },
                    {
                        "key": 1613520000000,
                        "key_as_string": "1613520000",
                        "doc_count": 9562,
                    },
                    {
                        "key": 1613606400000,
                        "key_as_string": "1613606400",
                        "doc_count": 8978,
                    },
                    {
                        "key": 1613692800000,
                        "key_as_string": "1613692800",
                        "doc_count": 7940,
                    },
                    {
                        "key": 1613779200000,
                        "key_as_string": "1613779200",
                        "doc_count": 6720,
                    },
                    {
                        "key": 1613865600000,
                        "key_as_string": "1613865600",
                        "doc_count": 5791,
                    },
                    {
                        "key": 1613952000000,
                        "key_as_string": "1613952000",
                        "doc_count": 7063,
                    },
                    {
                        "key": 1614038400000,
                        "key_as_string": "1614038400",
                        "doc_count": 6018,
                    },
                    {
                        "key": 1614124800000,
                        "key_as_string": "1614124800",
                        "doc_count": 5682,
                    },
                    {
                        "key": 1614211200000,
                        "key_as_string": "1614211200",
                        "doc_count": 5383,
                    },
                    {
                        "key": 1614297600000,
                        "key_as_string": "1614297600",
                        "doc_count": 6458,
                    },
                    {
                        "key": 1614384000000,
                        "key_as_string": "1614384000",
                        "doc_count": 6413,
                    },
                    {
                        "key": 1614470400000,
                        "key_as_string": "1614470400",
                        "doc_count": 8971,
                    },
                ],
            },
        },
    },
}

activity_response = {
    "took": 3,
    "timed_out": False,
    "_shards": {"total": 9, "successful": 9, "skipped": 0, "failed": 0},
    "hits": {
        "total": {"value": 10000, "relation": "gte"},
        "max_score": None,
        "hits": [],
    },
    "aggregations": {
        "channelusername": {
            "doc_count_error_upper_bound": 344,
            "sum_other_doc_count": 22187,
            "buckets": [
                {"key": "PatriotsEEUU", "doc_count": 15432},
                {"key": "QStorm1111", "doc_count": 10680},
                {"key": "q_anons", "doc_count": 2996},
                {"key": "Wahrheit2020", "doc_count": 2166},
                {"key": "stopthestea", "doc_count": 1857},
                {"key": "thewhiterabbitnews", "doc_count": 1715},
                {"key": "wwg1wga_info", "doc_count": 1678},
                {"key": "q_proofs", "doc_count": 1270},
                {"key": "TheTrumpist", "doc_count": 1246},
                {"key": "FreeWestVirginia", "doc_count": 862},
            ],
        }
    },
    "aggby_key": "channelusername",
}


class TestNdjsonFormatter:
    @pytest.fixture
    def content(self):
        formatter = NdjsonFormatter(content_response)
        return formatter.content()

    @pytest.fixture
    def timeseries(self):
        formatter = NdjsonFormatter(timeseries_response)
        return formatter.timeseries()

    @pytest.fixture
    def activity(self):
        formatter = NdjsonFormatter(activity_response)
        return formatter.activity("channelusername")

    def test_content_length(self, content):
        assert len(content) == 8

    def test_content_keys(self, content):
        keys = sorted(
            set(itertools.chain.from_iterable(json.loads(x).keys() for x in content))
        )
        assert keys == [
            "action",
            "archivedon",
            "channelabout",
            "channeltitle",
            "channelusername",
            "date",
            "editdate",
            "edithide",
            "entities",
            "forwards",
            "fromid",
            "fwdfrom",
            "groupedid",
            "media",
            "message",
            "peerid",
            "pinned",
            "post",
            "postauthor",
            "reformattedkey",
            "replies",
            "replyto",
            "silent",
            "userinfo",
            "v",
            "viabotid",
            "views",
        ]

    def test_timeseries_length(self, timeseries):
        assert len(timeseries) == 31

    def test_timeseries_keys(self, timeseries):
        keys = sorted(
            set(itertools.chain.from_iterable(json.loads(x).keys() for x in timeseries))
        )
        assert keys == ["doc_count", "key", "key_as_string"]

    def test_activity_length(self, activity):
        assert len(activity) == 10

    def test_activity_keys(self, activity):
        keys = sorted(
            set(itertools.chain.from_iterable(json.loads(x).keys() for x in activity))
        )
        assert keys == ["doc_count", "key"]


class TestJsonFormatter:
    @pytest.fixture
    def content(self):
        formatter = JsonFormatter(content_response)
        return formatter.content()

    @pytest.fixture
    def timeseries(self):
        formatter = JsonFormatter(timeseries_response)
        return formatter.timeseries()

    @pytest.fixture
    def timeseries_changepoint(self):
        formatter = JsonFormatter(timeseries_with_changepoint_response)
        return formatter.timeseries()

    @pytest.fixture
    def activity(self):
        formatter = JsonFormatter(activity_response)
        return formatter.activity("channelusername")

    def test_content_length(self, content):
        assert len(json.loads(content[0])["results"]) == 8

    def test_content_keys(self, content):
        keys = sorted(
            set(
                itertools.chain.from_iterable(
                    x.keys() for x in json.loads(content[0])["results"]
                )
            )
        )
        assert keys == [
            "action",
            "archivedon",
            "channelabout",
            "channeltitle",
            "channelusername",
            "date",
            "editdate",
            "edithide",
            "entities",
            "forwards",
            "fromid",
            "fwdfrom",
            "groupedid",
            "media",
            "message",
            "peerid",
            "pinned",
            "post",
            "postauthor",
            "reformattedkey",
            "replies",
            "replyto",
            "silent",
            "userinfo",
            "v",
            "viabotid",
            "views",
        ]

    def test_timeseries_length(self, timeseries):
        assert len(json.loads(timeseries[0])["results"]) == 31

    def test_timeseries_keys(self, timeseries):
        keys = sorted(
            set(
                itertools.chain.from_iterable(
                    x.keys() for x in json.loads(timeseries[0])["results"]
                )
            )
        )
        assert keys == ["doc_count", "key", "key_as_string"]

    def test_timeseries_with_changepoint_length(self, timeseries_changepoint):
        assert len(json.loads(timeseries_changepoint[0])["results"]) == 54
        assert (
            len(json.loads(timeseries_changepoint[0])["changepoint"]["date"]["before"])
            == 21
        )
        assert (
            len(json.loads(timeseries_changepoint[0])["changepoint"]["date"]["after"])
            == 33
        )

    def test_timeseries_with_changepoint_keys(self, timeseries_changepoint):
        keys = set(
            itertools.chain.from_iterable(
                x.keys() for x in json.loads(timeseries_changepoint[0])["results"]
            )
        )
        keys = keys.union(
            set([x for x in json.loads(timeseries_changepoint[0])["changepoint"]])
        )
        assert sorted(keys) == [
            "change_date",
            "date",
            "doc_count",
            "key",
            "key_as_string",
        ]

    def test_activity_length(self, activity):
        assert len(json.loads(activity[0])["results"]) == 10

    def test_activity_keys(self, activity):
        keys = sorted(
            set(
                itertools.chain.from_iterable(
                    x.keys() for x in json.loads(activity[0])["results"]
                )
            )
        )
        assert keys == ["doc_count", "key"]
